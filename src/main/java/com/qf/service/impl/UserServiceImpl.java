package com.qf.service.impl;

import com.qf.mapper.UserMapper;
import com.qf.service.UserService;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {


    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public String findById(Integer id) {
        return userMapper.findById(id);
    }
}

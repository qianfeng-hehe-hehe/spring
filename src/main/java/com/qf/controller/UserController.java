package com.qf.controller;

import com.qf.service.UserService;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {

    private UserService userService;
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    public String findById(Integer id){
        return userService.findById(id);
    }
}
